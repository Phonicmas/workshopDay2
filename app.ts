import {
  mistService,
  Envelope,
  postToRapid,
} from "@mist-cloud-eu/mist-tools-ts";
import { STOCK } from "./STOCK";
import { PRODUCTS } from "./PRODUCTS";
import { USERS } from "./USERS";

mistService({
  "product-service": (envelope: Envelope<{ productId: string, userId: string }>) => {
    let product = PRODUCTS.find((x) => x.id === envelope.payload.productId);
    postToRapid("reply", product);
  },
  "user-service": (envelope: Envelope<{ productId: string, userId: string }>) => {
    let user = USERS.find((x) => x.id === envelope.payload.userId);
    postToRapid("user-event", {
      locationId: user?.preferredLocation,
       productId: envelope.payload.productId,
    });
  },
  "stock-service": (envelope: Envelope<{ locationId: string, productId: string }>) => {
    let stock = STOCK.find((x) => 
      x.product === envelope.payload.productId &&
      x.location === envelope.payload.locationId);
    postToRapid("reply", {stock: stock?.stock});
  },

});


