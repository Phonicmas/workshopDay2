"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mist_tools_ts_1 = require("@mist-cloud-eu/mist-tools-ts");
const STOCK_1 = require("./STOCK");
const PRODUCTS_1 = require("./PRODUCTS");
const USERS_1 = require("./USERS");
(0, mist_tools_ts_1.mistService)({
    "product-service": (envelope) => {
        let product = PRODUCTS_1.PRODUCTS.find((x) => x.id === envelope.payload.productId);
        (0, mist_tools_ts_1.postToRapid)("reply", product);
    },
    "user-service": (envelope) => {
        let user = USERS_1.USERS.find((x) => x.id === envelope.payload.userId);
        (0, mist_tools_ts_1.postToRapid)("user-event", {
            locationId: user === null || user === void 0 ? void 0 : user.preferredLocation,
            productId: envelope.payload.productId,
        });
    },
    "stock-service": (envelope) => {
        let stock = STOCK_1.STOCK.find((x) => x.product === envelope.payload.productId &&
            x.location === envelope.payload.locationId);
        (0, mist_tools_ts_1.postToRapid)("reply", { stock: stock === null || stock === void 0 ? void 0 : stock.stock });
    },
});
